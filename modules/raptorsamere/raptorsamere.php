<?php 
if(!defined('_PS_VERSION_'))
	exit;

/**
* Classe principale
*/
class RaptorSaMere extends Module
{
	
	public function __construct()
	{
		$this->name = 'raptorsamere';
		$this->tab = 'front_office_features';
		$this->version = '1.0.0';
		$this->author = 'Nicolas Leroux';
		$this->need_instance = 0;
		$this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
		$this->bootstrap = true;

		parent::__construct();

		$this->displayName = $this->l('RAPTORSAMERE');
		$this->description = $this->l('Module permetant de RAPTORSAMERE la page');

		$this->confirmUninstall = $this->l('Êtes-vous sur de vouloir supprimer ce module ?');

		if(!Configuration::get('RAPTORSAMERE_NAME'))
			$this->warning = $this->l('Pas de nom renseigné.');
	}


	public function install(){

		if(Shop::isFeatureActive())
			Shop::setContext(Shop::CONTEXT_ALL);

		if(!parent::install() ||
			!$this->registerHook('footer') ||
			!$this->registerHook('rightColumn') ||
			!$this->registerHook('header') ||
			!Configuration::updateValue('RAPTORSAMERE_NAME', 'my friend')
		)
			return false;
		return true;
	}

	public function uninstall(){
		if(!parent::uninstall() || 
			!Configuration::deleteByName('RAPTORSAMERE_NAME')
		)
			return false;
		return true;
	}

	public function hookDisplayFooter($params){
		$this->context->smarty->assign(
			array(
				'my_module_name' => Configuration::get('RAPTORSAMERE_NAME'),
				'my_module_link' => $this->context->link->getModuleLink('RAPTORSAMERE', 'display')
			)
		);
		return $this->display(__FILE__, 'RAPTORSAMERE.tpl');
	}

	public function hookDisplayRightColumn($params){
		return $this->hookDisplayFooter($params);
	}

	public function hookDisplayHeader(){
		$this->context->controller->addJS($this->_path.'js/jquery.raptorize.1.0.js', 'all');
		$this->context->controller->addJS($this->_path.'js/jquery-1.4.1.min.js', 'all');
	}
}
